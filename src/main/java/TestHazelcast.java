import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;

public class TestHazelcast {

    public static void main(String[] args) {
        HazelcastInstance hz1 = Hazelcast.newHazelcastInstance();
        HazelcastInstance hz2 = Hazelcast.newHazelcastInstance();
        HazelcastInstance hz3 = Hazelcast.newHazelcastInstance();
        IMap<String, String> map = hz1.getMap("test");
        for (int i = 0; i < 1000; i++) {
            map.put(i + "", "поставте будь ласка залік");
        }
    }

}
