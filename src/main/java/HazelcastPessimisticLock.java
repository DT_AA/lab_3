import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;

import java.util.Map;

public class HazelcastPessimisticLock {

    public static final String MAP_NAME = "pessimistic";
    public static final String KEY = "key";

    public static void main(String[] args) {
        HazelcastInstance hz = Hazelcast.newHazelcastInstance();
        IMap<String, Integer> map = hz.getMap(MAP_NAME);
        updateValues_pessimisticLock(map);
    }

    private static void updateValues_pessimisticLock(IMap<String, Integer> map) {
        map.putIfAbsent(KEY, 0);
        for (int i = 0; i < 1000; i++) {
            map.lock(KEY);
            int oldValue = map.get(KEY);
            int newValue = oldValue + 1;
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
            map.put(KEY, newValue);
            System.out.println(newValue + " put to the map");
            map.unlock(KEY);
        }
    }

}
