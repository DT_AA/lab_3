import com.hazelcast.collection.IQueue;
import com.hazelcast.config.Config;
import com.hazelcast.config.QueueConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.concurrent.TimeUnit;

public class QueueWriter {

    public static final String QUEUE_NAME = "test";

    public static void main(String[] args) throws Exception {
        QueueConfig queueConfig = new QueueConfig().setMaxSize(10).setName(QUEUE_NAME);
        Config config = new Config().addQueueConfig(queueConfig);
        HazelcastInstance hz = Hazelcast.newHazelcastInstance(config);
        IQueue<String> queue = hz.getQueue(QUEUE_NAME);
        for (int i = 0; i < 20; i++) {
            boolean operationResult = queue.offer("message " + i, 1, TimeUnit.HOURS);
            System.out.println(i + "" + operationResult);
        }
    }

}
