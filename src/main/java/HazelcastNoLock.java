import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.Map;

public class HazelcastNoLock {
    public static final String MAP_NAME = "noLock";
    public static final String KEY = "key";

    public static void main(String[] args) {
        HazelcastInstance hz = Hazelcast.newHazelcastInstance();
        Map<String, Integer> map = hz.getMap(MAP_NAME);
        updateValues_noLock(map);
    }

    private static void updateValues_noLock(Map<String, Integer> map) {
        map.putIfAbsent(KEY, 0);
        for (int i = 0; i < 1000; i++) {
            int oldValue = map.get(KEY);
            int newValue = oldValue + 1;
            try {
                Thread.sleep(20);
            } catch (Exception e) {
                e.printStackTrace();
            }
            map.put(KEY, newValue);
            System.out.println(newValue + " put to the map");
        }
    }

}
