import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.EntryProcessor;
import com.hazelcast.map.IMap;

import java.util.Map;

public class HazelcastOptimisticLock {

    public static final String MAP_NAME = "optimistic";
    public static final String KEY = "key";

    public static void main(String[] args) {
        HazelcastInstance hz = Hazelcast.newHazelcastInstance();
        IMap<String, Integer> map = hz.getMap(MAP_NAME);
        updateValues_optimisticLock(map);
    }

    private static void updateValues_optimisticLock(IMap<String, Integer> map) {
        map.putIfAbsent(KEY, 0);
        for (int i = 0; i < 1000; i++) {
            while (true) {
                Integer oldValue = map.get(KEY);
                int newValue = oldValue + 1;
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ((boolean) map.executeOnKey(KEY, new EntryReplaceProcessor(oldValue, newValue))) {
                    System.out.println(newValue + " put to the map");
                    break;
                } else {
                    System.out.println("value is updated by another process. try again");
                }
            }
        }
    }

}

class EntryReplaceProcessor implements EntryProcessor {

    private final int oldVal;
    private final int replacement;

    public EntryReplaceProcessor(int oldVal, int replacement) {
        this.oldVal = oldVal;
        this.replacement = replacement;
    }

    @Override
    public Object process(Map.Entry entry) {
        if (entry.getValue().equals(this.oldVal)) {
            entry.setValue(replacement);
            return true;
        }
        return false;
    }

    @Override
    public EntryProcessor getBackupProcessor() {
        return EntryProcessor.super.getBackupProcessor();
    }
}
